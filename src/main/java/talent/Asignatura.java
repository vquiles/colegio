package talent;

public class Asignatura {
	private String nombre;
	private String siglas;
	private int horasLectivas;

	public Asignatura(String _nombre, String _siglas, int _horasLectivas) {
		this.nombre = _nombre;
		this.siglas = _siglas;
		this.horasLectivas = _horasLectivas;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String value) {
		this.nombre = value;
	}

	public String getSiglas() {
		return this.siglas;
	}

	public void setSiglas(String value) {
		this.siglas = value;
	}

	public int getHorasLectivas() {
		return this.horasLectivas;
	}

	public void setHorasLectivas(int value) {
		this.horasLectivas = value;
	}
}
