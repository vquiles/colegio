package talent;

import talent.enums.Sexo;
import talent.exception.AsignaturaExistenteException;
import talent.exception.DatoVacioException;
import talent.exception.SexoInicialIncorrectoException;

public class Colegio {
	private Asignatura[] asignaturas;
	protected Profesor[] profesores;
	private Alumno[] alumnos;
	private int contadorAsignaturas;
	private int[] contadorPersonas;

	private final String opcionSalir = "6";
	private final String opcionSalirVincular = "VOLVER";
	private final String opcionSalirMostrar = "VOLVER";
	private final int limiteAsignaturas = 20;
	private final int limitePersonas[] = { 10, 20 };
	private final String literalesTipoPersona[] = { "profesor", "alumno" };

	public Colegio() {
		this.asignaturas = new Asignatura[this.limiteAsignaturas];
		this.profesores = new Profesor[this.limitePersonas[0]];
		this.alumnos = new Alumno[this.limitePersonas[1]];
		this.contadorAsignaturas = 0;
		this.contadorPersonas = new int[2];
		this.contadorPersonas[0] = 0;
		this.contadorPersonas[1] = 0;
	}

	public void gestionarColegio() throws DatoVacioException {
		String opcionNivel1 = null;
		do {
			this.imprimirMenuPrincipal();
			opcionNivel1 = Utils.pedirStringPorPantalla();
			switch (opcionNivel1) {
			case "1":
				this.crearAsignatura();
				break;
			case "2":
				this.crearPersona(0);
				break;
			case "3":
				this.crearPersona(1);
				break;
			case "4":
				this.mostrarMenuVincular();
				break;
			case "5":
				this.mostrarMenuMostrar();
				break;
			}
		} while (!opcionSalir.equals(opcionNivel1));
	}

	protected void mostrarMenuMostrar() throws DatoVacioException {
		String opcionMenuMostrar = null;
		do {
			this.imprimirMenuMostrar();
			opcionMenuMostrar = Utils.pedirStringPorPantalla();
			switch (opcionMenuMostrar) {
			case "1":
				this.mostrarInformacionPersona(0);
				break;
			case "2":
				this.mostrarInformacionPersona(1);
				break;
			}
		} while (!this.opcionSalirMostrar.equals(opcionMenuMostrar));
	}

	private void mostrarInformacionPersona(int tipo) throws DatoVacioException {
		Persona persona;

		persona = this.obtenerPersona(tipo);
		if (persona != null) {
			System.out.println("Nombre: " + persona.getNombre());
			System.out.println("Apellidos: " + persona.getApellidos());
			System.out.println("Edad: " + persona.getEdad());
			System.out.println("Sexo: " + persona.getSexo());
			System.out.println("Listado de asignaturas");
			System.out.println("=======================");
			for (Asignatura a : persona.getAsignaturas()) {
				if (a != null) {
					System.out.println("\t" + a.getNombre() + "(" + a.getSiglas() + ") de " + a.getHorasLectivas()
							+ " horas lectivas");
				}
			}
			System.out.println();
			System.out.println();
		} else {
			System.out.println("No se ha encontrado a la persona");
		}

	}

	private void mostrarMenuVincular() throws DatoVacioException {
		String opcionMenuVincular = null;
		do {
			this.imprimirMenuVincular();
			opcionMenuVincular = Utils.pedirStringPorPantalla();
			switch (opcionMenuVincular) {
			case "1":
				this.vincularAsignaturaPersona(0);
				break;
			case "2":
				this.vincularAsignaturaPersona(1);
				break;
			}
		} while (!opcionSalirVincular.equals(opcionMenuVincular));
	}

	private void vincularAsignaturaPersona(int tipo) throws DatoVacioException {
		Persona persona;
		Asignatura asignatura;

		persona = this.obtenerPersona(tipo);
		if (persona != null) {
			System.out.println("Vinculado asignaturas a: " + persona.getNombre() + " " + persona.getApellidos());
			asignatura = this.pedirAsignatura();

			try {
				persona.vincularAsignatura(asignatura);
				System.out.println(
						asignatura.getNombre() + " de " + asignatura.getHorasLectivas() + " horas lectivas vinculada");
			} catch (AsignaturaExistenteException e) {
				System.out.println("La asignatura no se ha podido insertar porque ya exist�a para la persona");
			}
		} else {
			System.out.println("No se ha encontrado a la persona");
		}

	}

	private Asignatura pedirAsignatura() throws DatoVacioException {
		String siglasAsignatura = null;
		Asignatura asignatura = null;

		do {
			System.out.println("Introduce las siglas de la asignatura");
			siglasAsignatura = Utils.pedirStringPorPantalla();
			asignatura = this.obtenerAsignaturaPorSilgas(siglasAsignatura);
		} while (asignatura == null);

		return asignatura;
	}

	private Asignatura obtenerAsignaturaPorSilgas(String siglasAsignatura) {
		Asignatura asignaturaDevolver = null;
		for (Asignatura asignatura : this.asignaturas) {
			if (asignatura != null && siglasAsignatura.equals(asignatura.getSiglas())) {
				asignaturaDevolver = asignatura;
				break;
			}
		}
		return asignaturaDevolver;
	}

	public Persona obtenerPersona(int tipo) throws DatoVacioException {
		Persona persona = null;
		String dni;
		System.out.println("Introduzca el DNI del " + this.literalesTipoPersona[tipo]);
		dni = Utils.pedirStringPorPantalla();

		persona = this.buscarPersonaEnArray(tipo, dni);

		return persona;
	}

	private Persona buscarPersonaEnArray(int tipo, String dni) {
		Persona personaDevolver = null;
		Persona listaPersonas[];
		
		if (tipo == 0) {
			listaPersonas = this.profesores;
		} else {
			listaPersonas = this.alumnos;
		}
		for (Persona persona : listaPersonas) {
			if (persona != null && persona.getDni().equals(dni)) {
				personaDevolver = persona;
				break;
			}
		}
		return personaDevolver;
	}

	private void crearPersona(int tipo) {
		Persona persona = null;
		String nombre = null;
		String apellidos = null;
		String dni = null;
		int edad = 0;
		Sexo sexo = null;
		boolean correcto;

		if (this.contadorPersonas[tipo] >= this.limitePersonas[tipo]) {
			System.out.println("No se pueden crear m�s asignaturas");
		} else {

			do {
				try {
					correcto = true;
					System.out.println("Creando " + this.literalesTipoPersona[tipo] + "...");
					System.out.print("Introduce Nombre:");
					nombre = Utils.pedirStringPorPantalla();
					System.out.print("Introduce Apellidos:");
					apellidos = Utils.pedirStringPorPantalla();
					System.out.print("Introduce DNI:");
					dni = Utils.pedirStringPorPantalla();
					System.out.print("Introduce Sexo(M/F):");
					sexo = Sexo.getSexoPorInicial(Utils.pedirStringPorPantalla().toUpperCase());
					System.out.print("Introduce Edad:");
					edad = Utils.pedirEnteroPorPantalla();
				} catch (DatoVacioException | NumberFormatException | SexoInicialIncorrectoException e) {
					correcto = false;
				}

			} while (!correcto);
			// TODO: Falta comprobar que no hay repetidos
			if (tipo == 0) {
				persona = new Profesor(nombre, apellidos, dni, edad, sexo);
				this.profesores[this.contadorPersonas[tipo]] = (Profesor) persona;
				System.out.println("�Profesor Creado!");
			} else {
				persona = new Alumno(nombre, apellidos, dni, edad, sexo);
				this.alumnos[this.contadorPersonas[tipo]] = (Alumno) persona;
				System.out.println("�Alumno Creado!");
			}
			this.contadorPersonas[tipo]++;
		}

	}

	private void crearAsignatura() throws DatoVacioException {
		String nombre;
		String siglas;
		int horasLectivas = 0;
		Asignatura asignatura;
		boolean correcto = true;
		if (this.contadorAsignaturas >= this.limiteAsignaturas) {
			System.out.println("No se pueden crear m�s asignaturas");
		} else {
			do {
				correcto = true;
				System.out.println("Creando asignaturas...");
				System.out.print("Introduce Nombre:");
				nombre = Utils.pedirStringPorPantalla();
				System.out.print("Introduce Siglas:");
				siglas = Utils.pedirStringPorPantalla();

				System.out.print("Introduce Horas Lectivas:");
				try {
					horasLectivas = Utils.pedirEnteroPorPantalla();
				} catch (NumberFormatException e) {
					correcto = false;
				}
				if (Utils.esCadenaVaciaNula(nombre) || Utils.esCadenaVaciaNula(siglas)) {
					correcto = false;
				}
			} while (!correcto);
			// TODO: Validar que no exista asignatura con esas siglas
			asignatura = new Asignatura(nombre, siglas, horasLectivas);
			this.asignaturas[this.contadorAsignaturas] = asignatura;
			this.contadorAsignaturas++;
			System.out.println("�Asignatura Creada!");
		}
	}

	private void imprimirMenuPrincipal() {
		System.out.println();
		System.out.println("Bienvenido al gestor de colegios. �Qu� opci�n quieres elegir?");
		System.out.println("\t1.- Crear Asignaturas");
		System.out.println("\t2.- Crear Profesores");
		System.out.println("\t3.- Crear Alumnos");
		System.out.println("\t4.- Vincular");
		System.out.println("\t5.- Mostrar datos persona");
		System.out.println("\t6.- Salir");
	}

	private void imprimirMenuVincular() {
		System.out.println();
		System.out.println("Selecciona que quieres vincular");
		System.out.println("\t1.- Vincular a profesores");
		System.out.println("\t2.- Vincular a alumnos");
		System.out.println("\tVOLVER.- Volver al men� anterior");
	}

	private void imprimirMenuMostrar() {
		System.out.println();
		System.out.println("Selecciona que quieres vincular");
		System.out.println("\t1.- Mostrar informaci�n de profesores");
		System.out.println("\t2.- Mostrar informaci�n de alumnos");
		System.out.println("\tVOLVER.- Volver al men� anterior");
	}
}
