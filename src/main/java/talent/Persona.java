package talent;

import talent.enums.Sexo;
import talent.exception.AsignaturaExistenteException;

public abstract class Persona {
	protected String nombre;
	protected String apellidos;
	protected String dni;
	protected int edad;
	protected Sexo sexo;
	protected Asignatura[] asignaturas;
	protected int contadorAsignaturas;

	public Persona(String _nombre, String _apellidos, String _dni, int _edad, Sexo _sexo, int maxAsig) {
		this.nombre = _nombre;
		this.apellidos = _apellidos;
		this.dni = _dni;
		this.edad = _edad;
		this.sexo = _sexo;
		this.contadorAsignaturas = 0;
		this.asignaturas = new Asignatura[maxAsig];
	}

	protected void vincularAsignatura(Asignatura asignatura) throws AsignaturaExistenteException {
		//TODO Validar que cabe la asignatura
		for (Asignatura a : this.asignaturas) {
			if (a != null && a.getSiglas().equals(asignatura.getSiglas())) {
				throw new AsignaturaExistenteException();
			}
		}
		this.asignaturas[this.contadorAsignaturas] = asignatura;
	}
	
	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String value) {
		this.nombre = value;
	}

	public String getApellidos() {
		return this.apellidos;
	}

	public void setApellidos(String value) {
		this.apellidos = value;
	}

	public String getDni() {
		return this.dni;
	}

	public void setDni(String value) {
		this.dni = value;
	}

	public int getEdad() {
		return this.edad;
	}

	public void setEdad(int value) {
		this.edad = value;
	}

	public Sexo getSexo() {
		return this.sexo;
	}

	public void setSexo(Sexo value) {
		this.sexo = value;
	}

	public Asignatura[] getAsignaturas() {
		return this.asignaturas;
	}

	public void setAsignaturas(Asignatura[] value) {
		this.asignaturas = value;
	}

}
