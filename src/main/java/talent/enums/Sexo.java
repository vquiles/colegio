package talent.enums;

import talent.exception.SexoInicialIncorrectoException;

public enum Sexo {
	MASCULINO, FEMENINO;

	public static Sexo getSexoPorInicial(String inicial) throws SexoInicialIncorrectoException {
		Sexo salida = null;
		if ("M".equals(inicial)) {
			salida = Sexo.MASCULINO;
		} else if ("F".equals(inicial)) {
			salida = Sexo.FEMENINO;
		} else {
			throw new SexoInicialIncorrectoException();
		}
		return salida;
	}
}
